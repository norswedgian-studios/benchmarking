//
//  StressTest.swift
//  BenchmarkingTests
//
//  Created by Tim Fuqua on 12/27/21.
//  Copyright © Norswedgian Studios. All rights reserved.
//

import Foundation

final class StressTest {
    enum Defaults {
        static let iterations: Int = 10000
        static let runs: Int = 3
    }
    
    struct Result: CustomStringConvertible {
        var iterations: Int
        var runTimes: [TimeInterval]
        var fastest: TimeInterval { runTimes.min() ?? 0 }
        var slowest: TimeInterval { runTimes.max() ?? 0 }
        var average: TimeInterval { runTimes.reduce(TimeInterval(0), +) / TimeInterval(runTimes.count) }
        
        var description: String {
            """
            \titerations: \(iterations)
            \trunTimes: \(runTimes.map({ String(format: "%.3f", $0) }))
            \tfastest: \(String(format: "%.3f", fastest))
            \tslowest: \(String(format: "%.3f", slowest))
            \taverage: \(String(format: "%.3f", average))
            """
        }
    }
    
    private let iterations: Int
    private let runs: Int
        
    init(iterations: Int = Defaults.iterations, runs: Int = Defaults.runs) {
        self.iterations = iterations
        self.runs = runs
    }
    
    func run(code: @escaping (() -> Void)) -> Result {
        var results: [TimeInterval] = []
        
        (0..<runs).forEach { _ in
            let start = CFAbsoluteTimeGetCurrent()
            (0..<iterations).forEach { _ in code() }
            let end = CFAbsoluteTimeGetCurrent()
            results.append(end - start)
        }

        return Result(iterations: iterations, runTimes: results)
    }
}
