//
//  DefaultStressTestExampleTests.swift
//  BenchmarkingTests
//
//  Created by Tim Fuqua on 12/27/21.
//  Copyright © Norswedgian Studios. All rights reserved.
//

import XCTest
@testable import Benchmarking

enum Factorializer {
    static func bruteForceFactorial(of number: Int) -> Int {
        if number < 0 { return -1 }
        if number == 0 { return 0 }
        if number == 1 { return 1 }
        
        return number * Factorializer.bruteForceFactorial(of: number - 1)
    }
    
    static func factorialOfFifteen() -> Int { 1307674368000 }
}

class DefaultStressTestExampleTests: BenchmarkTests {}

extension DefaultStressTestExampleTests {
    func test_bruteForceFactorial_ofTen() {
//        let number = Factorializer.bruteForceFactorial(of: 15)
//        XCTAssertEqual(number, 1307674368000)

        print(
            stressTester.run {
                let number = Factorializer.bruteForceFactorial(of: 15)
                XCTAssertEqual(number, 1307674368000)
            }
        )
    }
    
    func test_factorialOfFifteen() {
//        let number = Factorializer.factorialOfFifteen()
//        XCTAssertEqual(number, 1307674368000)

        print(
            stressTester.run {
                let _ = Factorializer.factorialOfFifteen()
            }
        )
    }
}
