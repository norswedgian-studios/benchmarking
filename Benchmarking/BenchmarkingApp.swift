//
//  BenchmarkingApp.swift
//  Benchmarking
//
//  Created by Tim Fuqua on 12/27/21.
//  Copyright © Norswedgian Studios. All rights reserved.
//

import SwiftUI

@main
struct BenchmarkingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
