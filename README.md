# Benchmarking

A skeleton framework with a base XCTestCase class that contains a StressTest object that can be used to execute code for a set number of iterations and a set number of runs, and compiles the results into a formatted, easy to read string.

Your test cases subclass the base class, and you can override the default stress tester if desired in order to change the number of iterations or number of runs to perform.
